package game;

import java.util.ArrayList;
import java.awt.Point;

public class Agressive implements Strategy {

	@Override
	public boolean checkfor(ArrayList<Race> races, CommandInvoker command) {

		// get location of the doctor, always first in the ArrayList
		Point thedoctor = races.get(0).getLocation();
		// arraylist for enemies on samesquare
		ArrayList<Race> samesquare = new ArrayList<Race>();
		// adding doctor's enemies to samesquare ArrayList if they are on the
		// same square
		for (int r = 1; r < races.size(); r++) {
			if (races.get(r).getLocation().equals(thedoctor)) {
				samesquare.add(races.get(r));
			}
		}

		// if there is no enemies, return false
		if (samesquare.size() == 0) {
			return false;
		} else if (samesquare.size() < 3) {
			// Destroy the enemies if it is 1 or 2 on the same square
			command.Do(new Destroycm(races, samesquare));
			//System.out.println("Total: " + races.size() + "\tdestroyed: "
				//	+ samesquare.size());
			return false;
		} else {
			// more than 2 enemies, return true and game over
			//System.out.println("GAME OVER");
			return true;
		}
	}
}
