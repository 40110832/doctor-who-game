package game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class WindowGame implements Window {

	private JPanel mainPanel, commandsPanel, battleground;
	private JButton travelbtn, undobtn, playbtn;
	private JRadioButton passive, agressive;
	private ButtonGroup btngroup;
	private Square[][] squares;
	
	@Override
	public JPanel run(GUI g) {
		
		g.setGameState(new WindowStart());

		squares = new Square[WindowStart.getVs()][WindowStart.getVs()];
		mainPanel = new JPanel();
		commandsPanel = new JPanel();
		battleground = new JPanel();
		
		   
				g.setSize(800, 720);
				mainPanel.add(commandsPanel);
				mainPanel.add(battleground);
				mainPanel.setBackground(Color.GRAY);

				// Move Button
				travelbtn = new JButton("travel");
				travelbtn.addActionListener(travelClick());

				// Undo Button
				undobtn = new JButton("undo");
				undobtn.addActionListener(undoClick());
				undobtn.setEnabled(false);

				playbtn = new JButton("play");
				playbtn.addActionListener(playClick());
				playbtn.setEnabled(true);

				// Passive Radio Button
				passive = new JRadioButton("Passive");
				passive.addItemListener(passiveSelected());
				passive.setSelected(true);

				// Aggressive Radio Button
				agressive = new JRadioButton("Aggressive");
				agressive.addItemListener(aggressiveSelected());

				// ButtonGroup for Passive & Aggressive Radio Buttons
				btngroup = new ButtonGroup();
				btngroup.add(passive);
				btngroup.add(agressive);

				// Setup the controlPanel

				commandsPanel.add(travelbtn);
				commandsPanel.add(undobtn);
				commandsPanel.add(playbtn);
				commandsPanel.add(new JLabel("Choose between modes"));
				commandsPanel.add(passive);
				commandsPanel.add(agressive);

				// Setting the panel color
				battleground.setBackground(Color.BLACK);
				// getting the input variable size(Vs) and setting the GridLayout
				battleground.setLayout(new GridLayout(WindowStart.getVs(), WindowStart.getVs(), 0, 0));
				// setting the size based on the Vs and multiply it by 80 in order to
				// create the panel
				battleground.setPreferredSize(new Dimension(WindowStart.getVs() * 80, WindowStart
						.getVs() * 80));
				// creating the squares based on Vs and adding observer to each
				// individual square
				for (int x = 0; x < WindowStart.getVs(); x++) {
					for (int y = 0; y < WindowStart.getVs(); y++) {
						squares[x][y] = new Square(Game.getGame(), new Point(x, y));			 
						Game.getGame().addObserver(squares[x][y]);
						battleground.add(squares[x][y]);
					}
				}

				return mainPanel;
	}

	// Creating Item Listener for Passive Radio Button
		private ItemListener passiveSelected() {
			return new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					// if it is changed
					if (e.getStateChange() == 1) {
						System.out.println("Passive checked");
						// game will change the strategy to Passive
						Game.getGame().setStrategy(
								new Passive());
					}
				}
			};
		}

		// Creating Item Listener for Aggressive Radio Button
		private ItemListener aggressiveSelected() {
			return new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					// if it is changed
					if (e.getStateChange() == 1) {
						System.out.println("Aggressive checked");
						// game will change the strategy to Aggressive
						Game.getGame().setStrategy(
								new Agressive());

					}
				}
			};
		}

		// Action listener for move button
		private ActionListener travelClick() {
			return new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// execute move method
					Game.getGame().move();
					undobtn.setEnabled(true);
				}
			};
		}

		// Action listener for undo button
		private ActionListener undoClick() {
			return new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// execute undo method
					Game.getGame().undo();
					// execute undo method and checking that undo command is
					// available
					if (!Game.getGame().checkUndoisAvailable())
						undobtn.setEnabled(false);

				}
			};
		}

		// Action listener for play button
		private ActionListener playClick() {
			return new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// execute playsound method
					Game.getGame().playsound();
					playbtn.setEnabled(false);
				}
			};
		}

	}
	

