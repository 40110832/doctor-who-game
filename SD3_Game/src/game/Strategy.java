package game;
import java.util.ArrayList;

public interface Strategy {
	public boolean checkfor(ArrayList<Race> races, CommandInvoker command);
}
