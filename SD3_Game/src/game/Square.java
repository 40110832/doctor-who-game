package game;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class Square extends JPanel implements Observer {
	
	Game game;
	Point location;
	
	//JPanel skyPanel = new JPanel();
	JLabel[] l = new  JLabel[4];
	
	public Square(Game game, Point location) {
		this.game = game;
		this.location = location;
		
		// Style square
		setBorder(BorderFactory.createLineBorder(Color.darkGray));
		setOpaque(false);
		
		// Setup the 2x2 grid
		setLayout(new GridLayout(2, 2, 0, 0));
		for (int i=0; i<4; i++) {
			l[i] = new JLabel();
			this.add(l[i]);
		}
	}
	
	//when observed object change the following method is called
	public void update(Observable obj, Object arg) {
		// Get the ships in this location(square)
		ArrayList<Race> races = game.getRaces(location);
		
		// Clear the 2x2 grid
		for (int i=0; i<4; i++) {
			l[i].setIcon(null);
		}
		
		// Add the Ships to the 2x2 grid
		for (int i=0; i<races.size() ; i++) {
			l[i].setIcon(races.get(i).getIcon());
		}
		
		// Always put the MasterShip in the top left square
		if (races.contains(TheDoctor.getTheDoctor())) {
			l[0].setIcon(races.get(0).getIcon());
		}
	}
}
