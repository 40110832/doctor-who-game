package game;
import java.util.Random;

public class EnemiesFactory {
	public static Enemies createEnemy() {

		Enemies e = null;
		Random rand = new Random();

		// Randomly select the Enemy
		switch (rand.nextInt(3)) {
		case 0:
			e = new Daleks();
			break;
		case 1:
			e = new Cybermen();
			break;
		case 2:
			e = new WeepingAngels();
			break;
		}
		
		return e;
	}
}
