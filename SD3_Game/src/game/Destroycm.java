package game;

import java.util.ArrayList;

public class Destroycm implements Command {

	ArrayList<Race> race, racedestroyed;

	public Destroycm(ArrayList<Race> race, ArrayList<Race> racedestroyed) {
		this.race = race;
		this.racedestroyed = racedestroyed;
	}

	public void Do() {
		race.removeAll(racedestroyed);

	}

	public void Undo() {
		race.addAll(racedestroyed);

	}

}
