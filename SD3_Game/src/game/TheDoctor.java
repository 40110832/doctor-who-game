package game;

import java.awt.Point;
import java.util.Random;
import javax.swing.ImageIcon;

public class TheDoctor extends Race {

	private static TheDoctor thedoctor;

	private TheDoctor() {
		// creating icon
		icon = new ImageIcon("E:\\Ivo\\Doctor Who\\doctor_40px.png");

		int xloc, yloc;
		// initialize the ship
		Random r = new Random();
		xloc = r.nextInt(WindowStart.getVs());
		if (xloc == 0) {
			yloc = r.nextInt(WindowStart.getVs()-1) + 1;
		} else {
			yloc = r.nextInt(WindowStart.getVs());
		}

		location = new Point(xloc, yloc);
	}

	// Singleton TheDoctor
	public static synchronized TheDoctor getTheDoctor() {
		if (thedoctor == null)
			thedoctor = new TheDoctor();

		return thedoctor;
	}
}
