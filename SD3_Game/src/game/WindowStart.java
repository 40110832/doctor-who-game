package game;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;


public class WindowStart implements Window {

	private JPanel mainPanel;
    private JTextField  j ;
    public static int  vs;
    
	@SuppressWarnings("serial")
	@Override
	public JPanel run(GUI g) {
		
		g.setGameState(new WindowGame());
		mainPanel = new JPanel() {
	    };
	    
	    g.setSize(300, 300);
	    mainPanel.setBackground(Color.GRAY);
	    
	    // Doctor's Who Label
	    JLabel lbldwho = new JLabel("Doctor Who");
	    lbldwho.setFont(new Font(null, Font.PLAIN, 55));
	    lbldwho.setForeground(Color.white);
	    mainPanel.add(lbldwho);
	    
	    JLabel lbldwho2 = new JLabel("Choose size (4-8)");
	    lbldwho2.setFont(new Font(null, Font.PLAIN, 20));
	    lbldwho2.setForeground(Color.white);
	    mainPanel.add(lbldwho2);
	    
	    JButton help = new JButton("Help");
	    help.addActionListener(helpClick());
	    help.setFont(new Font( null, Font.PLAIN, 20));
	    mainPanel.add(help);
	    
	    j = new JTextField();
	    j.setPreferredSize(new Dimension(70,45));
	    mainPanel.add(j);
	   
	  		// Start Button
		JButton start = new JButton("Start");
		start.addActionListener(startClick());
		start.setFont(new Font( null, Font.PLAIN, 20));
		mainPanel.add(start);
				
		return mainPanel;
	}
	
	// Start ActionListener
		private ActionListener startClick() {
			return new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		try {  
	        		vs  = Integer.parseInt(j.getText());         		
	        		Game.getGame().play(); //execute play        
	        		}
	        		catch (NumberFormatException nfe) {
	        		
	        		}
	        	}
	        };
		}
		
		private ActionListener helpClick() {
			return new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        	GUI2 g = new GUI2();
	        	g.run();
	        	}
	        };
		}

	
    public static int getVs() {
		if(vs<4 || vs>8){	
			return 4;
		}
		else
		{
		return vs;
		}
	}
}
