package game;

import java.awt.Point;
import java.util.Random;
import javax.swing.ImageIcon;

public abstract class Race {

	protected Point location;
	protected ImageIcon icon;

	// travel method
	public void travel() {
		Random rand = new Random();
		int xloc, yloc;

		do {
			xloc = rand.nextInt(3) - 1; //xloc between -1 and 1
			yloc = rand.nextInt(3) - 1; //xloc between -1 and 1
		} 
		while ( xloc == 0  && yloc == 0 || !(location.x + xloc >= 0 && location.x + xloc <= WindowStart.getVs()-1
						&& location.y + yloc >= 0 && location.y + yloc <= WindowStart.getVs()-1));

		location.translate(xloc, yloc);
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}

	public ImageIcon getIcon() {
		return icon;
	}

	public void setIcon(ImageIcon icon) {
		this.icon = icon;
	}

}
