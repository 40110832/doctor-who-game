package game;

import java.awt.Point;
import java.util.ArrayList;

public class Passive implements Strategy {

	@Override
	public boolean checkfor(ArrayList<Race> races, CommandInvoker command) {

		// get location of the doctor, always first in the ArrayList
		Point thedoctor = races.get(0).getLocation();

		// arraylist for enemies on samesquare
		ArrayList<Race> samesquare = new ArrayList<Race>();

		// adding doctor's enemies to the samesquare ArrayList if they are on the
		// same square
		for (int r = 1; r < races.size(); r++) {
			if (races.get(r).getLocation().equals(thedoctor)) {
				samesquare.add(races.get(r));
			}
		}

		// if there is no enemies, return false
		if (samesquare.size() == 0) {
			return false;
		} else if (samesquare.size() == 1) {
			// Destroy the enemy if it is 1
			command.Do(new Destroycm(races, samesquare));
			//System.out.println("Total: " + races.size() + "\tdestroyed: "
				//	+ samesquare.size());
			return false;
		} else {
			// more than 1 enemy, return true and game over
			//System.out.println("GAME OVER");
			return true;
		}
	}
}
