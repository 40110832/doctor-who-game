package game;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class GUI extends JFrame {

	private Window window = new WindowStart();

	public GUI() {
		super("Doctor Who's Adventures");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
	}

	// Display the next window
	public void run() {
		setContentPane(window.run(this));
		setVisible(true);
	}

	// Set the next window
	public void setGameState(Window window) {
		this.window = window;
	}
}