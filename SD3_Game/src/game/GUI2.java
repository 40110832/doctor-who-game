package game;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Font;

@SuppressWarnings("serial")
public class GUI2 extends JFrame {

	private JPanel contentPane;

	public void run() {

		GUI2 frame = new GUI2();
		frame.setVisible(true);
	}

	public GUI2() {
		// setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 525, 400);
         
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel l = new JLabel("Doctor Who's Advetures");
		l.setForeground(Color.WHITE);
		l.setFont(new Font(null, Font.BOLD, 14));
		l.setBounds(157, 11, 182, 42);
		contentPane.add(l);

		JLabel l2 = new JLabel(
				"<html>Doctor Who's Advetures is a game in which you can fight against doctor's enemies."
						+ "The aim is to defeat all the enemies, you will face the scariest creatures in the"
						+ " universe, be cautious or you will have to regenerate more than once.<html>");
		l2.setForeground(Color.WHITE);
		l2.setBounds(25, 48, 460, 66);
		contentPane.add(l2);

		JLabel l3 = new JLabel("Instructions");
		l3.setFont(new Font(null, Font.BOLD, 14));
		l3.setForeground(Color.WHITE);
		l3.setBounds(189, 125, 112, 14);
		contentPane.add(l3);

		JLabel l4 = new JLabel(
				"<html>First you have to specify the size of the world, this will generate the world."
						+ "You can choose between 4 and 8, if you specify less or more, the world will be 4 by 4 this if the default size.<br>"
						+ "</html>");
		l4.setForeground(Color.WHITE);
		l4.setBounds(25, 149, 388, 66);
		contentPane.add(l4);
		
		JLabel l5 = new JLabel("<html>Inside the game you can press travel button or undo button. Pressing trevel will "
				+ "move the doctor and all enemies. Pressing undo button will step you back one move"
				+ " along with all enemies.<br><br>"
				+ "During the game you can press passive or agressive button. Pressing agressive will make "
				+ "you defeat two enemies at once. Also you can press passive and you will be again able to"
				+ "defeat only one enemy at once. Another feature is play button. The button starts doctor who's"
				+ " theme music. </html>");
		l5.setForeground(Color.WHITE);
		l5.setBounds(25, 208, 407, 164);
		contentPane.add(l5);

	}
}
