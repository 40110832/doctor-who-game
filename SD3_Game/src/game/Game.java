package game;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Random;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.awt.Point;
import java.io.File;

public class Game extends Observable {

	private GUI g;
	private static Game game;
	private ArrayList<Race> races;
	private CommandInvoker command;
	private Strategy strategy;

	public static void main(String[] args) {
		Game.getGame();
	}

	private Game() {
		g = new GUI();
		g.setGameState(new WindowStart());// game begins
		g.run();
	}

	// Singleton
	public static synchronized Game getGame() {
		if (game == null) {
			game = new Game();
		}
		return game;
	}

	public void play() {
		races = new ArrayList<Race>();
		command = new CommandInvoker();
		strategy = new Passive();
		races.add(TheDoctor.getTheDoctor());
		g.run();

		 setChanged(); //if it is true
		// Draw updates
		 notifyObservers("Play");
	}

	public void move() {
		command.Do(new Travelcm(races));

		// Spawn an enemy (1/3 chance)
		Random rand = new Random();
		if (rand.nextInt(3) == 2)
			command.Do(new Spawncm(races));

		// Check MasterShip square for other space ships
		if (this.strategy.checkfor(races, command)) {
			// Next state (End)
			g.run();
		} else {
			// Draw updates
			notifyObservers("move");
		}
	}

	// Undo last commands
	public void undo() {
		command.Undo();

		// Draw updates
		notifyObservers("undo");
	}

	public void playsound() {
		new Thread() {
			@Override
			public void run() {
				try {
					File file = new File("E:\\Ivo\\Doctor Who\\DoctorWho3.wav");
					Clip clip = AudioSystem.getClip();
					clip.open(AudioSystem.getAudioInputStream(file));
					clip.start();
					Thread.sleep(clip.getMicrosecondLength());

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}.start();
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Set the OperationalStrategy - strategy
	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	// Get the ships at a Point
	public ArrayList<Race> getRaces(Point location) {
		ArrayList<Race> samesquare = new ArrayList<Race>();
		for (Race r : races) {
			if (r.getLocation().equals(location)) {
				samesquare.add(r);
			}
		}
		return samesquare;
	}

	public void setChangedTrue() {
		setChanged(); // return true if observed objects have changed
	}

	// Proxy the CommandManager isUndoAvailable method.
	public boolean checkUndoisAvailable() {
		return command.checkUndoisAvailable();
	}
}
