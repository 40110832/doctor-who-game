package game;

import java.util.ArrayList;

public class Spawncm implements Command {

	ArrayList<Race> races;
	Enemies enemy;

	public Spawncm(ArrayList<Race> races) {
		this.races = races;
	}

	@Override
	public void Do() {
		//Creating enemy and adding it to Arraylist of races
		enemy = EnemiesFactory.createEnemy();
		races.add(enemy);
	}

	@Override
	public void Undo() {
		//removing enemy from Arraylist of races
		races.remove(enemy);
	}
}
