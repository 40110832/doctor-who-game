package game;

import javax.swing.JPanel;

public interface Window {
	public JPanel run(GUI g);
}
