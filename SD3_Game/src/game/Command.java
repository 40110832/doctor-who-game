package game;

public interface Command {
	public void Do();
    public void Undo();
}
