package game;

import java.awt.Point;
import java.util.ArrayList;


public class Travelcm implements Command {

	ArrayList<Race> races;
	ArrayList<Point> lastlocation;
	
	public Travelcm(ArrayList<Race> races) {
		this.races = races;
		this.lastlocation = new ArrayList<Point>();
	}

	@Override
	public void Do() {
		// for all in arraylist of races
		for (int i = 0; i < races.size(); i++) {
			// Saving x,y coordinates of Arraylist current obj in races inside
			// lastlocation
			this.lastlocation.add((Point) races.get(i).getLocation().clone());
			// execute travel method and move all objects
			this.races.get(i).travel();

			// Set the changed flag to true
			 Game.getGame().setChangedTrue();
		}
	}

	@Override
	public void Undo() {
		// for all in arraylist of races
		for (int i = 0; i < races.size(); i++) {
			// location will be set back using the x,y coordinates saved in
			// lastlocation
			this.races.get(i).setLocation(lastlocation.get(i));

			// Set the changed flag to true
			 Game.getGame().setChangedTrue();
		}

	}
}
