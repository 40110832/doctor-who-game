package game;

import java.util.ArrayList;

public class CommandInvoker {
	private ArrayList<Command> commmandshistory = new ArrayList<Command>();

	// Do method
	public void Do(Command command) {
		// adding the command to the history
		this.commmandshistory.add(command);
		// Do the command
		command.Do();
	}

	// Checking whether the history is empty
	public boolean checkUndoisAvailable() {
		return !commmandshistory.isEmpty();
	}

	// Undo method
	public void Undo() {
		if (checkUndoisAvailable()) {
			// execute Undo method
			this.commmandshistory.remove(commmandshistory.size()-1).Undo();
		}
	}
}
